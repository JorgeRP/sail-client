import SSO from "./app";
import CrossStorageService from "../services/crossStorage/crossStorage.service";
import EventHandleService from "../services/eventHandler/eventHandler.service";

describe("SSO", () => {
  it("should call CrossStorageService connect", () => {
    spyOn(CrossStorageService, "connect");
    SSO.init();
    expect(CrossStorageService.connect).toHaveBeenCalled();
  });

  it("should call EventHandleService connect", () => {
    spyOn(EventHandleService, "listen");
    SSO.init();
    expect(EventHandleService.listen).toHaveBeenCalled();
  });
});
