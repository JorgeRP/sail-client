import SSOClient from "../ssoClient/ssoClient.controller";
import CrossStorageService from "../services/crossStorage/crossStorage.service";
import EventHandlerService from "../services/eventHandler/eventHandler.service";
import Commons from "../services/commons/commons.service";
import { KEYWORDS, EVENTS, LOGS } from "../constants";
import moment from "moment";

const clientsso = new SSOClient();

function init() {
  Commons.debugLog(
    `[${moment().format("DD/MM/YYYY HH:mm")}] ${LOGS.SSO_TAG} ${
      LOGS.DEBUG_MODE
    }`
  );
  const callback = function() {
    clientsso.receiveMessage.apply(clientsso);
  };

  const closeListener = function() {
    EventHandlerService.close(KEYWORDS.MESSAGE, callback);
  };

  CrossStorageService.connect();
  EventHandlerService.listen(KEYWORDS.MESSAGE, callback);
  EventHandlerService.listen(EVENTS.DONE, closeListener);
}

export default {
  init
};
