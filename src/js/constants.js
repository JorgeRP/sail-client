export const COGNITO_STORAGE_KEYS = {
  ENCODED: {
    ID_TOKEN: "isdcTto",
    LAST_AUTH_USER: "Lsacstt",
    ACCESS_TOKEN: "ascccte",
    REFRESH_TOKEN: "rsecftr"
  },
  DECODED: {
    ID_TOKEN: "idToken",
    LAST_AUTH_USER: "LastAuthUser",
    ACCESS_TOKEN: "accessToken",
    REFRESH_TOKEN: "refreshToken"
  }
};

export const KEYWORDS = {
  EXPIRATION_ACCESS_TOKEN: "exp",
  COOKIES: "cookies",
  NO_COOKIES: "no_sct_cookie",
  MESSAGE: "message",
  DEBUG_MODE: "levelLog=3"
};

export const EVENTS = {
  LOGIN: "sctLogin",
  LOGOUT: "sctLogout",
  DONE: "sct-sso-done"
};

export const ENDPOINTS = {
  LOGIN: "/sct/login",
  LOGOUT: "/sct/logout"
};

export const LOGS = {
  DEBUG_MODE: "Debug mode...",
  SSO_TAG: "[SSO]"
};
