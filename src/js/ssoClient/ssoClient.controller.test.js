import SSOClient from "./ssoClient.controller";
import Commons from "../services/commons/commons.service";
import { ENDPOINTS } from "../constants";

describe("SSOClient", () => {
  const ssoClient = new SSOClient();

  const tokenExample = `eyJraWQiOiI5czZHa2xpNjhCR1J5eW5PdDFMbEJKakt3SGE4RXJUU1l4NURGUz
        ZUbFpBPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI3NTEwMzk2ZS1kMGIwLTRiZTEtOTE4Ny0
        2MjRmZjIyZDQ2ZDMiLCJhdWQiOiIzYTlxc2xjMjI2OWExazJ0OGJzM3IyNGdjOCIsInRva2VuX
        3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTA4MzkyNjYwLCJpc3MiOiJodHRwczpcL1wvY29nbml0
        by1pZHAuZXUtd2VzdC0xLmFtYXpvbmF3cy5jb21cL2V1LXdlc3QtMV9JR3VTUUE1eEIiLCJjb2du
        aXRvOnVzZXJuYW1lIjoidmljdG9yLnNhZXpAZGlhZ3JvdXAuY29tIiwiZXhwIjoxNTA4Mzk2MjYwL
        CJjdXN0b206bGVnYWxfY29uZF9hY2NlcHRlZCI6IjMiLCJpYXQiOjE1MDgzOTI2NjAsImN1c3RvbTp
        pZF9jdXN0b21lciI6IjAwMDAwMDAwMDAwMUMifQ.KxzUQkNQaop0vMwST3WgigwIFg2gKPxgCqj5hli
        NUhmzq0oXAYKRL1agnAH0Y_b7JC1FLNGY0L8YriUyhezOLfS59mbIMf_Jz_Qml8NJ1wRaJGtPTAruJwC
        IJ0V13voPYimCMM-XSVMbynX4eTDRlTZcYjsLPUCN7r8-3uwbiOG_kHZy8FFQO2lFemZH4aQUGabT4AQ0
        un4ckaiO_5xRBgybTzHeDD4zg8EwthjkplBjer9UgpMd7bmR1k2yIiqtuHAXJI8eUKyijAwyGjpi-ebr5m
        uvRjhikiO2806s6PY2KlPg8HkmIjr-sdgdhfyut6ynjgb`;

  it("should call sctLogin if it is defined", () => {
    window.sctLogin = function() {
      return true;
    };
    window.event = {
      data: {
        key: "cookies",
        value: `asccte=${tokenExample}`
      }
    };

    spyOn(window, "sctLogin");
    ssoClient.receiveMessage();
    expect(window.sctLogin).toHaveBeenCalled();
  });

  it("should call commons redirect if sctLogin is not defined", () => {
    window.sctLogin = null;

    window.event = {
      data: {
        key: "cookies",
        value: `asccte=${tokenExample}`
      }
    };

    spyOn(Commons, "redirectTo");
    ssoClient.receiveMessage();
    expect(Commons.redirectTo).toHaveBeenCalledWith(ENDPOINTS.LOGIN);
  });

  it("should call sctLogout if it is defined", () => {
    window.sctLogout = function() {
      return true;
    };

    window.event = {
      data: {
        key: "no_sct_cookie"
      }
    };

    window.document.cookie = "ascccte=target";

    spyOn(window, "sctLogout");
    ssoClient.receiveMessage();
    expect(window.sctLogout).toHaveBeenCalled();
  });

  it("should call commons redirect if sctLogout is not defined", () => {
    window.sctLogout = null;

    window.event = {
      data: {
        key: "no_sct_cookie"
      }
    };
    document.cookie = `ascccte=${tokenExample}`;

    spyOn(Commons, "redirectTo");
    ssoClient.receiveMessage();
    expect(Commons.redirectTo).toHaveBeenCalledWith(ENDPOINTS.LOGOUT);
  });
});
