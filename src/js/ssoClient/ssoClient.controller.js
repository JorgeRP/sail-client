import Commons from "../services/commons/commons.service";
import CookieService from "../services/cookie/cookie.service";
import EventHandlerService from "../services/eventHandler/eventHandler.service";
import { EVENTS, KEYWORDS, ENDPOINTS } from "../constants";

export default class SSOClient {
  postLoginAction() {
    //lanza evento de login
    EventHandlerService.dispatch(new Event(EVENTS.LOGIN));

    //checkea si existe la función cargada por el cliente y si hay cookies en el dominio
    if (
      typeof sctLogin === "function" &&
      !CookieService.hasSctCookies(document.cookie)
    ) {
      //setea cookies en el dominio actual
      CookieService.setCookies(CookieService.formatCookies(event.data.value));
      //si existe, se ejecuta
      sctLogin();
    } else if (
      typeof sctLogin !== "function" &&
      !CookieService.hasSctCookies(document.cookie)
    ) {
      //setea cookies en el dominio actual
      CookieService.setCookies(CookieService.formatCookies(event.data.value));
      //url a la que redirige tras setear las cookies y en caso de que no exista la función cargada por el cliente
      Commons.redirectTo(ENDPOINTS.LOGIN);
    }
    EventHandlerService.dispatch(new Event(EVENTS.DONE));
  }

  postLogoutAction() {
    //lanza evento de aviso de logout
    EventHandlerService.dispatch(new Event(EVENTS.LOGOUT));
    //checkea si existe la función de logout cargada por el cliente
    if (
      typeof sctLogout === "function" &&
      CookieService.hasSctCookies(document.cookie)
    ) {
      //borra las cookies
      CookieService.deleteCookies();
      //borra el localStorage
      Commons.clearLocalStorage();
      //si existe, se ejecuta
      sctLogout();
    } else if (
      typeof sctLogout !== "function" &&
      CookieService.hasSctCookies(document.cookie)
    ) {
      //borra las cookies
      CookieService.deleteCookies();
      //borra el localStorage
      Commons.clearLocalStorage();
      //redirección si la función del cliente no existe
      Commons.redirectTo(ENDPOINTS.LOGOUT);
    }
    EventHandlerService.dispatch(new Event(EVENTS.DONE));
  }

  receiveMessage() {
    if (event.data.key && event.data.key === KEYWORDS.COOKIES) {
      this.postLoginAction();
    } else if (event.data.key && event.data.key === KEYWORDS.NO_COOKIES) {
      this.postLogoutAction();
    }
  }
}
