export default class eventHandler {
  static listen(key, callback) {
    window.addEventListener(key, callback);
  }

  //emit calls postMessage, which is correct for send events through differente domains
  static emit(data, target) {
    window.postMessage(data, target);
  }

  //to send events to same window/document/origin we use dispatchEvent
  static dispatch(event) {
    window.dispatchEvent(event);
  }

  static close(key, callback) {
    window.removeEventListener(key, callback);
  }
}
