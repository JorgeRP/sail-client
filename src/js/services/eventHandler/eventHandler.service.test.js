import EventHandler from "./eventHandler.service";

describe("EventHandlerService", () => {
  it("should add event listener", () => {
    spyOn(window, "addEventListener");
    EventHandler.listen();
    expect(window.addEventListener).toHaveBeenCalled();
  });

  it("should send post message", () => {
    spyOn(window, "postMessage");
    EventHandler.emit();
    expect(window.postMessage).toHaveBeenCalled();
  });

  it("should dispatch event", () => {
    spyOn(window, "dispatchEvent");
    const event = new Event("test_event");
    EventHandler.dispatch(event);
    expect(window.dispatchEvent).toHaveBeenCalled();
  });

  it("should close listener", () => {
    spyOn(window, "removeEventListener");
    EventHandler.close("message", function() {});
    expect(window.removeEventListener).toHaveBeenCalled();
  });
});
