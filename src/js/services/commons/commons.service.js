import { COGNITO_STORAGE_KEYS, KEYWORDS } from "../../constants";
import moment from "moment";

export default class Commons {
  static timestampToGMTDate(timestamp) {
    const a = new Date(timestamp * 1000);
    return a;
  }

  static redirectTo(url) {
    window.location.href = url;
  }

  static clearLocalStorage() {
    Object.keys(COGNITO_STORAGE_KEYS.ENCODED).forEach(key => {
      localStorage.removeItem(COGNITO_STORAGE_KEYS.ENCODED[key]);
    });
  }

  static debugLog(log) {
    //esto es un tributo a Garretus
    window.location.search.includes(KEYWORDS.DEBUG_MODE) &&
      console.log(`[${moment().format("DD/MM/YYYY HH:mm")}] ${log}`);
  }
}
