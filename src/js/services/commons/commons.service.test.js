import Commons from "./commons.service";
import { COGNITO_STORAGE_KEYS } from "../../constants";

describe("Commons", () => {
  it("should return a date format", () => {
    const timestamp = 1519196525;
    const date = Commons.timestampToGMTDate(timestamp);
    expect(date).toBe(date);
  });

  // it("should redirect", () => {
  //   Commons.redirectTo("http://localhost:4200");
  //   expect(window.location.href).toEqual("http://localhost:9876/context.html");
  // });

  it("should call localstorage with every single sct key", () => {
    spyOn(localStorage, "removeItem");
    Commons.clearLocalStorage();
    expect(localStorage.removeItem).toHaveBeenCalledWith(
      COGNITO_STORAGE_KEYS.ENCODED.ACCESS_TOKEN
    );
    expect(localStorage.removeItem).toHaveBeenCalledWith(
      COGNITO_STORAGE_KEYS.ENCODED.ID_TOKEN
    );
    expect(localStorage.removeItem).toHaveBeenCalledWith(
      COGNITO_STORAGE_KEYS.ENCODED.LAST_AUTH_USER
    );
    expect(localStorage.removeItem).toHaveBeenCalledWith(
      COGNITO_STORAGE_KEYS.ENCODED.REFRESH_TOKEN
    );
  });
});
