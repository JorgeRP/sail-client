import { COGNITO_STORAGE_KEYS, KEYWORDS } from "../../constants";
import Commons from "../commons/commons.service";
import JwtDecoder from "../jwtDecoder/jwtDecoder.service";
import Cookies from "js-cookie";

export default class CookieService {
  static setCookies(cookies) {
    const possibleValues = [
      COGNITO_STORAGE_KEYS.ENCODED.ID_TOKEN,
      COGNITO_STORAGE_KEYS.ENCODED.LAST_AUTH_USER,
      COGNITO_STORAGE_KEYS.ENCODED.ACCESS_TOKEN,
      COGNITO_STORAGE_KEYS.ENCODED.REFRESH_TOKEN
    ];

    Object.keys(cookies).forEach(key => {
      if (possibleValues.includes(key)) {
        const cookieOptions = {
          secure: true,
          expires: Commons.timestampToGMTDate(
            JwtDecoder.decodeToken(
              cookies[COGNITO_STORAGE_KEYS.ENCODED.ACCESS_TOKEN]
            )[KEYWORDS.EXPIRATION_ACCESS_TOKEN]
          )
        };

        Cookies.set(key, cookies[key], cookieOptions);
      }
    });
  }

  static formatCookies(cookies) {
    cookies = cookies
      .replace(/;/g, ",")
      .replace(/\s/g, "")
      .replace(/=/g, ":")
      .split(",");
    for (let i = 0; i < cookies.length; i++) {
      cookies[i] = cookies[i].split(":");
    }
    const obj = {};
    cookies.forEach(function(data) {
      obj[data[0]] = data[1];
    });

    return obj;
  }

  static hasSctCookies(cookie) {
    if (
      cookie.includes(COGNITO_STORAGE_KEYS.ENCODED.ID_TOKEN) ||
      cookie.includes(COGNITO_STORAGE_KEYS.ENCODED.LAST_AUTH_USER) ||
      cookie.includes(COGNITO_STORAGE_KEYS.ENCODED.ACCESS_TOKEN) ||
      cookie.includes(COGNITO_STORAGE_KEYS.ENCODED.REFRESH_TOKEN)
    ) {
      return true;
    } else {
      return false;
    }
  }

  static deleteCookies() {
    Object.keys(COGNITO_STORAGE_KEYS.ENCODED).forEach(key => {
      Cookies.remove(COGNITO_STORAGE_KEYS.ENCODED[key]);
    });
  }
}
