import CookieService from "./cookie.service";
import Cookies from "js-cookie";
import { COGNITO_STORAGE_KEYS } from "../../constants";

describe("CookieService", () => {
  const tokenExample = `eyJraWQiOiI5czZHa2xpNjhCR1J5eW5PdDFMbEJKakt3SGE4RXJUU1l4NURGUz
        ZUbFpBPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI3NTEwMzk2ZS1kMGIwLTRiZTEtOTE4Ny0
        2MjRmZjIyZDQ2ZDMiLCJhdWQiOiIzYTlxc2xjMjI2OWExazJ0OGJzM3IyNGdjOCIsInRva2VuX
        3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTA4MzkyNjYwLCJpc3MiOiJodHRwczpcL1wvY29nbml0
        by1pZHAuZXUtd2VzdC0xLmFtYXpvbmF3cy5jb21cL2V1LXdlc3QtMV9JR3VTUUE1eEIiLCJjb2du
        aXRvOnVzZXJuYW1lIjoidmljdG9yLnNhZXpAZGlhZ3JvdXAuY29tIiwiZXhwIjoxNTA4Mzk2MjYwL
        CJjdXN0b206bGVnYWxfY29uZF9hY2NlcHRlZCI6IjMiLCJpYXQiOjE1MDgzOTI2NjAsImN1c3RvbTp
        pZF9jdXN0b21lciI6IjAwMDAwMDAwMDAwMUMifQ.KxzUQkNQaop0vMwST3WgigwIFg2gKPxgCqj5hli
        NUhmzq0oXAYKRL1agnAH0Y_b7JC1FLNGY0L8YriUyhezOLfS59mbIMf_Jz_Qml8NJ1wRaJGtPTAruJwC
        IJ0V13voPYimCMM-XSVMbynX4eTDRlTZcYjsLPUCN7r8-3uwbiOG_kHZy8FFQO2lFemZH4aQUGabT4AQ0
        un4ckaiO_5xRBgybTzHeDD4zg8EwthjkplBjer9UgpMd7bmR1k2yIiqtuHAXJI8eUKyijAwyGjpi-ebr5m
        uvRjhikiO2806s6PY2KlPg8HkmIjr-sdgdhfyut6ynjgb`;

  it("should set cookies", () => {
    const cookies = {
      ascccte: tokenExample
    };
    spyOn(Cookies, "set");
    CookieService.setCookies(cookies);
    expect(Cookies.set).toHaveBeenCalled();
  });

  it("should format cookies to object", () => {
    const unformatCookie = `ascccte=prueba`;
    const formatCookies = {
      ascccte: "prueba"
    };
    const cookies = CookieService.formatCookies(unformatCookie);
    expect(cookies).toEqual(formatCookies);
  });

  it("should delete cookies", () => {
    spyOn(Cookies, "remove");
    CookieService.deleteCookies();
    expect(Cookies.remove).toHaveBeenCalledWith(
      COGNITO_STORAGE_KEYS.ENCODED.ACCESS_TOKEN
    );
    expect(Cookies.remove).toHaveBeenCalledWith(
      COGNITO_STORAGE_KEYS.ENCODED.ID_TOKEN
    );
    expect(Cookies.remove).toHaveBeenCalledWith(
      COGNITO_STORAGE_KEYS.ENCODED.LAST_AUTH_USER
    );
    expect(Cookies.remove).toHaveBeenCalledWith(
      COGNITO_STORAGE_KEYS.ENCODED.REFRESH_TOKEN
    );
  });
});
