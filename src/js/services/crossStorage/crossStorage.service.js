import { CrossStorageClient } from "cross-storage";
import * as CONSTANTS from "../../constants";
import Commons from "../commons/commons.service";

export default class CrossStorageService {
  constructor() {
    this.storage;
  }
  static connect() {
    this.storage = new CrossStorageClient(process.env.HUB_URL, {
      timeout: 5000,
      frameId: "storageFrame"
    });

    this.onConnect();
  }

  static justConnect() {
    this.storage = new CrossStorageClient(process.env.HUB_URL, {
      timeout: 5000,
      frameId: "storageFrame"
    });
  }

  static onConnect() {
    this.storage
      .onConnect()
      .then(() => {
        return this.storage.getKeys();
      })
      .then(
        keys => {
          if (keys.length > 0) {
            this.formStoragePromises(keys, this.storage);
          } else {
            this.storage.close();
          }
        },
        error => {
          Commons.debugLog(error);
          this.storage.close();
        }
      );
  }

  static formStoragePromises(keys, storage) {
    const promises = [];
    keys.forEach(key => {
      if (this.isCognitoKey(key)) {
        promises.push(
          storage.get(key).then(value => {
            window.localStorage.setItem(key, value);
          })
        );
      }
    });
    Promise.all(promises).then(
      () => {
        storage.close();
      },
      error => {
        Commons.debugLog(error);
        storage.close();
      }
    );
  }

  static isCognitoKey(key) {
    if (
      key.includes(CONSTANTS.COGNITO_STORAGE_KEYS.ENCODED.ID_TOKEN) ||
      key.includes(CONSTANTS.COGNITO_STORAGE_KEYS.ENCODED.ACCESS_TOKEN) ||
      key.includes(CONSTANTS.COGNITO_STORAGE_KEYS.ENCODED.LAST_AUTH_USER) ||
      key.includes(CONSTANTS.COGNITO_STORAGE_KEYS.ENCODED.REFRESH_TOKEN)
    ) {
      return true;
    } else {
      return false;
    }
  }
}
