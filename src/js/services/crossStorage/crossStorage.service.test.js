import { CrossStorageClient } from "cross-storage";
import * as CONSTANTS from "../../constants";
import CrossStorageService from "./crossStorage.service";

describe("CrossStorageService", () => {
  const storage = {
    onConnect: () => {
      return new Promise(resolve => {
        resolve(true);
      });
    },
    get: () => {
      return new Promise(resolve => {
        resolve(true);
      });
    },
    set: () => {},
    close: () => {}
  };

  it("should create storage", () => {
    CrossStorageService.connect();
    expect(CrossStorageService.storage).toBeDefined();
  });

  it("isCognitoKey should return true", done => {
    storage.getKeys = function() {
      return new Promise(resolve => {
        resolve(["ascccte"]);
      });
    };
    CrossStorageService.storage = storage;
    CrossStorageService.onConnect();
    done();
    expect(CrossStorageService.storage).toBeDefined();
  });

  it("isCognitoKey should return false", done => {
    storage.getKeys = function() {
      return new Promise(resolve => {
        resolve([]);
      });
    };
    CrossStorageService.storage = storage;
    CrossStorageService.onConnect();
    done();
    expect(CrossStorageService.storage).toBeDefined();
  });

  it("key should return error", done => {
    storage.getKeys = function() {
      return new Promise((resolve, reject) => {
        reject("err");
      });
    };
    CrossStorageService.storage = storage;
    CrossStorageService.onConnect();
    done();
    expect(CrossStorageService.storage).toBeDefined();
  });
});
