import JwtDecoder from "./jwtDecoder.service";

describe("JwtDecoder", () => {
  it("sh0uld return case 0", () => {
    const str = "";
    JwtDecoder.urlBase64Decode(str);
    // break
  });

  it("sh0uld return case 0", () => {
    const str = "1234567";
    const newStr = JwtDecoder.urlBase64Decode(str);
    expect(newStr).toBeDefined();
  });

  it("sh0uld return case 0", () => {
    const str = "12";
    const newStr = JwtDecoder.urlBase64Decode(str);
    expect(newStr).toBeDefined();
  });

  it("should return null", () => {
    const expected = JwtDecoder.decodeToken(null);
    expect(expected).toEqual(null);
  });
});
