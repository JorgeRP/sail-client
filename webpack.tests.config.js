const ProgressBarPlugin = require("progress-bar-webpack-plugin");
const webpack = require("webpack");

module.exports = {
  plugins: [new ProgressBarPlugin()],
  devtool: "inline-source-map",
  plugins: [new ProgressBarPlugin()],
  module: {
    rules: [
      {
        test: /.js$/,
        exclude: [
          /node_modules(?!\/webpack-dev-server)/,
          /\.test\.js$/,
          /test.bundle\.js$/,
          /index\.js$/
        ],
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: ["env"]
            }
          },
          {
            loader: "istanbul-instrumenter-loader",
            query: {
              esModules: true
            }
          }
        ]
      }
    ]
  }
};
