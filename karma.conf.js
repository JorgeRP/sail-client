const webpackConfig = require("./webpack.tests.config.js");
const path = require("path");

module.exports = function(config) {
  config.set({
    basePath: "",
    frameworks: ["jasmine"],
    files: [
      {
        pattern: "src/js/test.bundle.js",
        watched: false
      }
    ],
    exclude: [],
    webpack: webpackConfig,
    webpackServer: {
      noInfo: true
    },
    preprocessors: {
      "src/js/test.bundle.js": ["webpack", "sourcemap"]
    },
    reporters: ["progress", "coverage-istanbul"],
    coverageIstanbulReporter: {
      reports: ["html", "lcovonly", "text-summary", "cobertura"],
      dir: path.join(__dirname, "coverage"),
      fixWebpackSourcePaths: true
    },
    port: 9876,
    colors: true,
    // values config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ["ChromeHeadless"],
    customLaunchers: {
      ChromeHeadless: {
        base: "Chrome",
        flags: ["--headless", "--remote-debugging-port=9222", "--disable-gpu"]
      },
      FirefoxHeadless: {
        base: "Firefox",
        flags: ["-headless"]
      }
    },
    singleRun: true,
    concurrency: Infinity
  });
};
