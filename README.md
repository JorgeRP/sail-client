# SCT SSO CLIENT

1. Requeriments
2. Instalation
3. Execution
4. Build

## Requeriments

nvm is recomended

node 7.5.0 (npm 4.1.2)

## Instalation

Execute npm install for dependencies

## Execution

Run npm run env-* where * can be:

- local
- dev
- test
- pro


## Build

Run npm run bundle-* where * can be:S

- local
- dev
- test
- pro
