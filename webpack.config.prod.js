const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ProgressBarPlugin = require("progress-bar-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const ZipPlugin = require("zip-webpack-plugin");
const BabelPlugin = require("babel-webpack-plugin");

const packageJson = require("./package.json");
const conf = require("./conf/prod.js");

module.exports = {
  entry: "./src/js/index.js",
  output: {
    path: path.join(__dirname, "dist"),
    filename: "[name].[hash:8].js",
    chunkFilename: "[name].[hash:8].js",
    libraryTarget: "var",
    library: "SSO"
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify(conf.NODE_ENV),
        HUB_URL: JSON.stringify(conf.HUB_URL)
      }
    }),
    new BabelPlugin({
      test: /\.js$/,
      presets: ["es2015"],
      sourceMaps: false,
      compact: false
    }),
    new webpack.optimize.UglifyJsPlugin(),
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      filename: "./index.html",
      hash: false,
      inject: true,
      compile: true,
      favicon: false,
      minify: false,
      cache: true,
      showErrors: true,
      chunks: "all",
      excludeChunks: [],
      xhtml: true
    }),
    new ProgressBarPlugin(),
    new CleanWebpackPlugin(["dist"]),
    new ZipPlugin({
      filename: `${packageJson.name}-${packageJson.version}.zip`
    })
  ]
};
